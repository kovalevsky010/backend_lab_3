import { dbConnection } from '../database/database.config';
import { EntityRepository } from '../interfaces';
import { User } from './user.entity';
import { CreateUserDto, UpdateUserDto } from './users.dto';

/**
 * Допишите оставшиеся методы учитывая пример с findAll
 */
class UsersRepository implements EntityRepository<User> {
  async findAll(): Promise<User[]> {
    try {
      /**
       * Соединение с БД лежит в обычной переменной
       * Чтобы достать репозиторий для сущности вызываю метод getRepository
       * Далее работаю уже с методами репозитория
       * Потыкайте все методы репозитория, их названия очень интуитивные
       * find, findOne, save, delete, update и проч
       */
      return dbConnection.getRepository(User).find();
    } catch (e) {
      throw e;
    }
  }

  async findOne(id: User['id']): Promise<User> {
    try {
      const user = await dbConnection.getRepository(User).findOne(id);

      if (!user) {
        throw Error(`User with id '${id}' not found.`);
      }

      return user;
    } catch (e) {
      throw e;
    }
  }

  async createOne(dto: CreateUserDto): Promise<User> {
    try {
      const user = {...dto} as User;
      return dbConnection.getRepository(User).save(user);
    } catch (e) {
      throw e;
    }
  }

  async updateOne(id: User['id'], dto: UpdateUserDto): Promise<User> {
    try {
      const updatedUser = {id, ...dto};
      return dbConnection.getRepository(User).save(updatedUser);
    } catch (e) {
      throw e;
    }
  }

  async deleteOne(id: User['id']): Promise<boolean> {
    try {
      const deletedResult = await dbConnection.getRepository(User).delete(id);
      const isSuccessfully = deletedResult.affected === 0 ?  false : true;
      return isSuccessfully;
    } catch (e) {
      return false
    }
  }
}

export const usersRepository = new UsersRepository();

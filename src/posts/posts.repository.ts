import e from 'express';
import { CreatePostDto, UpdatePostDto } from '.';
import { dbConnection } from '../database/database.config';
import { EntityRepository } from '../interfaces';
import { User } from '../users';
import { Post } from './post.entity';

/**
 * Тут все методы сами описываете, референс с UsersRepository
 */
class PostsRepository implements EntityRepository<Post> {
  async findAll(): Promise<Post[]> {
    try {
      return dbConnection.getRepository(Post).find();
    } catch (e) {
      throw e;
    }
  }

  async findOne(id: Post['id']): Promise<Post> {
    try {
      const post = await dbConnection.getRepository(Post).findOne(id);

      if (!post) {
        throw new Error(`Post with id '${id}' not found.`);
      }
    
      return post;
    } catch (e) {
      throw e;
    }
  }

  async findByUserId(id: User['id']): Promise<Post[]> {
    try {
      return dbConnection.getRepository(Post).find({where: {authorId: id}});
    } catch {
      throw e;
    }
  }

  async authorByPostId(id: Post['id']): Promise<User> {
    try {
      const post = await this.findOne(id);

      const author = await dbConnection.getRepository(User).findOne(post.authorId);

      if (!author) {
        throw new Error('This post has not author.');
      }

      return author;
    } catch (e) {
      throw e;
    }
  }

  async createOne(dto: CreatePostDto): Promise<Post> {
    try {
      const post = {...dto} as Post;
      return dbConnection.getRepository(Post).save(post);
    } catch (e) {
      throw e;
    }
  }

  async updateOne(id: Post['id'], dto: UpdatePostDto): Promise<Post> {
    try {
      const updatedPost = {id, ...dto};
      return dbConnection.getRepository(Post).save(updatedPost);
    } catch (e) {
      throw e;
    }
  }

  async deleteOne(id: Post['id']): Promise<boolean> {
    try {
      const deletedResult = await dbConnection.getRepository(Post).delete(id);
      const isSuccessfully = deletedResult.affected === 0 ? false : true;
      return isSuccessfully;
    } catch {
      throw e;
    }
  }
}

export const postsRepository = new PostsRepository();
